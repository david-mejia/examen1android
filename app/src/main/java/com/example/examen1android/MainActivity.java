package com.example.examen1android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre, etBase, etAltura;
    private Button btnLimpiar, btnSiguiente, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.etNombre);
        etBase = findViewById(R.id.etBase);
        etAltura = findViewById(R.id.etAltura);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        btnSalir = findViewById(R.id.btnSalir);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    irASiguienteActividad();
                } else {
                    Toast.makeText(MainActivity.this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void limpiarCampos() {
        etNombre.setText("");
        etBase.setText("");
        etAltura.setText("");
    }

    private boolean validarCampos() {
        return !etNombre.getText().toString().isEmpty() &&
                !etBase.getText().toString().isEmpty() &&
                !etAltura.getText().toString().isEmpty();
    }

    private void irASiguienteActividad() {
        Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
        intent.putExtra("nombre", etNombre.getText().toString());
        intent.putExtra("base", etBase.getText().toString());
        intent.putExtra("altura", etAltura.getText().toString());
        startActivity(intent);
    }
}
