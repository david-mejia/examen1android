package com.example.examen1android;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    private TextView tvNombre, tvBase, tvAltura, tvResultado;
    private RadioGroup rgOpciones;
    private Button btnCalcular, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        tvNombre = findViewById(R.id.tvNombre);
        tvBase = findViewById(R.id.tvBase);
        tvAltura = findViewById(R.id.tvAltura);
        tvResultado = findViewById(R.id.tvResultado);
        rgOpciones = findViewById(R.id.rgOpciones);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tvNombre.setText(extras.getString("nombre"));
            tvBase.setText(extras.getString("base"));
            tvAltura.setText(extras.getString("altura"));
        }

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcular() {
        String baseStr = tvBase.getText().toString();
        String alturaStr = tvAltura.getText().toString();

        if (!baseStr.isEmpty() && !alturaStr.isEmpty()) {
            float base = Float.parseFloat(baseStr);
            float altura = Float.parseFloat(alturaStr);

            Rectangulo rectangulo = new Rectangulo(base, altura);

            int selectedId = rgOpciones.getCheckedRadioButtonId();
            if (selectedId == R.id.rbArea) {
                tvResultado.setText("El Área es: " + rectangulo.calcularArea());
            } else if (selectedId == R.id.rbPerimetro) {
                tvResultado.setText("El Perímetro es: " + rectangulo.calcularPerimetro());
            } else {
                tvResultado.setText("Seleccione una opción");
            }
        } else {
            tvResultado.setText("Por favor, ingrese ambos valores");
        }
    }
}
